#import <Foundation/Foundation.h>


struct DataStruct
{
    int myDataInt;
    float myDataFloat;
    float* myDataFloatArray;
    float myDataFloatArrayLength;
};

extern "C" void _DataStructMethod(DataStruct myDataStruct)
{
    NSNumber* myInt = [NSNumber numberWithInt:myDataStruct.myDataInt];
    NSLog(@"Got data struct with int: %@, float %f", myInt, myDataStruct.myDataFloat);
    
    for (int i = 0; i < myDataStruct.myDataFloatArrayLength; i++) {
        NSNumber* arrayNumber = [NSNumber numberWithFloat:myDataStruct.myDataFloatArray[i]];
        NSLog(@"Got array number: %@", arrayNumber);
    }
}



