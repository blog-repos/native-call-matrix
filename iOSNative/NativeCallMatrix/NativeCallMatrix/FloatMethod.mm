#import <Foundation/Foundation.h>


extern "C" void _FloatMethod(float myFloat)
{
    NSNumber *myNumber = [NSNumber numberWithFloat: myFloat];
    NSLog(@"Got float number: %@", myNumber);
}
