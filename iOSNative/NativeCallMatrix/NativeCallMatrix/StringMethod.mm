#import <Foundation/Foundation.h>


extern "C" void _StringMethod(const char* myString)
{
    NSString* myNsString = [NSString stringWithUTF8String:myString];
    NSLog(@"Got passed string: %@", myNsString);
}
