#import <Foundation/Foundation.h>


extern "C" void _IntArrayMethod(int* array, int length)
{
    NSMutableArray* myArray = [NSMutableArray array];
    
    for (int i = 0; i < length; i++) {
        int arrayMember = array[i];
        [myArray addObject:[NSNumber numberWithInt:arrayMember]];
    }
    
    NSLog(@"Got int array with legth: %d and entries:", length);
    
    for (int i = 0; i < [myArray count]; i++) {
        NSNumber* number = [myArray objectAtIndex:i];
        NSLog(@"%@", number);
    }
}
