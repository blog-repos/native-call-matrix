package com.robinryf.nativecallmatrix;

public class FloatMethod {
    /*
     Make sure you use 'float' and not 'Float'. It is something completely different to the
     JNI bridge.
     */
    public static void call(float myFloat)
    {
        Log.i("Received float: " + myFloat);
    }
}
