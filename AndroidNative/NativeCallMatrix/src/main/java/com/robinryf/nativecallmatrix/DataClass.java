package com.robinryf.nativecallmatrix;

public class DataClass
{
    public int dataInt;
    public boolean dataBool;

    @Override
    public String toString() {
        return "Int: " + dataInt + ", Bool: " + dataBool;
    }
}

