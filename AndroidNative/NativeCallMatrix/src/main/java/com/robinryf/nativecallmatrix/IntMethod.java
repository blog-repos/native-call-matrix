package com.robinryf.nativecallmatrix;

public class IntMethod {
    /*
     Make sure you use 'int' and not 'Integer'. It is something completely different to the
     JNI bridge.
     */
    public static void call(int myInteger)
    {
        Log.i("Received int: " + myInteger);
    }
}
