package com.robinryf.nativecallmatrix;

public class IntArrayMethod {
    public static void call(int[] intArray)
    {
        Log.i("Received int array with length: " + intArray.length + " ints:");
        for (int i = 0; i < intArray.length; i++) {
            int element = intArray[i];
            Log.i(element + "\n");
        }

    }
}
