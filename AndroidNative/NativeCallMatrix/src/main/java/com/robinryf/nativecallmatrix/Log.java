package com.robinryf.nativecallmatrix;

import java.util.logging.Logger;

public class Log {

    private static Logger logger = Logger.getLogger("Unity.Native");

    public static void i(String message)
    {
        logger.info(message);
    }
}
