package com.robinryf.nativecallmatrix;

public class BoolMethod {
    /*
    Make sure you use 'boolean' and not 'Boolean'. It is something completely different to the
    JNI bridge.
    */
    public static void call(boolean myBoolean)
    {
        Log.i("Received bool: " + myBoolean);
    }
}
