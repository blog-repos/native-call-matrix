﻿using System.Runtime.InteropServices;

namespace Ios
{
    public static class IntMethod
    {
#if UNITY_EDITOR || UNITY_IOS
        [DllImport ("__Internal")]
        private static extern void _IntMethod(int myInt);
#endif
        
        public static void Call()
        {
#if UNITY_EDITOR || UNITY_IOS
            _IntMethod(42);
#endif
        }
    }
}