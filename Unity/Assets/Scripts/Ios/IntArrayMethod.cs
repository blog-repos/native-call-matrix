﻿using System.Runtime.InteropServices;

namespace Ios
{
    public static class IntArrayMethod
    {
#if UNITY_EDITOR || UNITY_IOS
        [DllImport ("__Internal")]
        private static extern void _IntArrayMethod( int[] myIntArray, int size);
#endif
        
        public static void Call()
        {
#if UNITY_EDITOR || UNITY_IOS
            var array = new[] {2, 4, 9, 10, 11};
            _IntArrayMethod(array, array.Length);
#endif
        }
    }
}