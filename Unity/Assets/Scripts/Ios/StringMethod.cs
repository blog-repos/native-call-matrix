﻿using System.Runtime.InteropServices;

namespace Ios
{
    public static class StringMethod
    {
#if UNITY_EDITOR || UNITY_IOS
        [DllImport ("__Internal")]
        private static extern void _StringMethod(string myString);
#endif
        
        public static void Call()
        {
#if UNITY_EDITOR || UNITY_IOS
            _StringMethod("Hello this is managed code.");
#endif
        }
    }
}