﻿using System.Runtime.InteropServices;

namespace Ios
{
    public static class DataStructMethod
    {
        [StructLayout(LayoutKind.Sequential)]
        private struct DataStruct
        {
            public int myDataInt;
            public float myDataFloat;
            public float[] myDataFloatArray;
            public float myDataFloatArrayLength;
        }
        
#if UNITY_EDITOR || UNITY_IOS
        [DllImport ("__Internal")]
        private static extern void _DataStructMethod(DataStruct myDataStruct);
#endif
        
        public static void Call()
        {
#if UNITY_EDITOR || UNITY_IOS
            var array = new[] {2.4f, 2.5f, 2.6f, 2.9f, 10.1f, 20.1f};
            var myDataStruct = new DataStruct
            {
                myDataInt = 32,
                myDataFloat = 19.3f,
                myDataFloatArray = array,
                myDataFloatArrayLength = array.Length
            };

            _DataStructMethod(myDataStruct);
#endif
        }
    }
}