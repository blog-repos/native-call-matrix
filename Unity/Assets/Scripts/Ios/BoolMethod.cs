﻿using System.Runtime.InteropServices;

namespace Ios
{
    public static class BoolMethod
    {
#if UNITY_EDITOR || UNITY_IOS
        [DllImport ("__Internal")]
        private static extern void _BoolMethod(bool myBool);
#endif
        
        public static void Call()
        {
#if UNITY_EDITOR || UNITY_IOS
            _BoolMethod(true);
#endif
        }
    }
}