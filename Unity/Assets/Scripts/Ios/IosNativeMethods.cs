﻿using UnityEngine;

namespace Ios
{
	public class IosNativeMethods : MonoBehaviour {

		private void Start ()
		{
			BoolMethod.Call();
			DataStructMethod.Call();
			FloatMethod.Call();
			IntArrayMethod.Call();
			IntMethod.Call();
			StringMethod.Call();
		}
	}
}
