﻿using System.Runtime.InteropServices;

namespace Ios
{
    public static class FloatMethod
    {
#if UNITY_EDITOR || UNITY_IOS
        [DllImport ("__Internal")]
        private static extern void _FloatMethod(float myFloat);
#endif
        
        public static void Call()
        {
#if UNITY_EDITOR || UNITY_IOS
            _FloatMethod(2.3f);
#endif
        }
    }
}