﻿using UnityEngine;

namespace Android
{
    public static class IntArrayMethod
    {
        public static void Call()
        {
#if UNITY_EDITOR || UNITY_ANDROID
            var nativeMethodsObj = new AndroidJavaObject("com.robinryf.nativecallmatrix.IntArrayMethod");
            nativeMethodsObj.CallStatic("call", new [] { 1, 2, 3 });
#endif
        }
    }
}