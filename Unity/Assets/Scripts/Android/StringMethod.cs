﻿using UnityEngine;

namespace Android
{
    public static class StringMethod
    {
        public static void Call()
        {
#if UNITY_EDITOR || UNITY_ANDROID
            var nativeMethodsObj = new AndroidJavaObject("com.robinryf.nativecallmatrix.StringMethod");
            nativeMethodsObj.CallStatic("call", "passedString");
#endif
        }
    }
}