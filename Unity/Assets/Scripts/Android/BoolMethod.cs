﻿using UnityEngine;

namespace Android
{
    public static class BoolMethod
    {
        public static void Call()
        {
#if UNITY_EDITOR || UNITY_ANDROID
            var nativeMethodsObj = new AndroidJavaObject("com.robinryf.nativecallmatrix.BoolMethod");
            nativeMethodsObj.CallStatic("call", true);
#endif
        }
    }
}
