﻿using UnityEngine;

namespace Android
{
    public class AndroidNativeMethods : MonoBehaviour
    {
        private void Start()
        {
            BoolMethod.Call();
            DataClassMethod.Call();
            FloatMethod.Call();
            IntArrayMethod.Call();
            IntMethod.Call();
            StringMethod.Call();
        }
    }
}
