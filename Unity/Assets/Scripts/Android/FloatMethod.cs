﻿using UnityEngine;

namespace Android
{
    public static class FloatMethod
    {
        public static void Call()
        {
#if UNITY_EDITOR || UNITY_ANDROID
            var nativeMethodsObj = new AndroidJavaObject("com.robinryf.nativecallmatrix.FloatMethod");
            nativeMethodsObj.CallStatic("call", 2.0f);
#endif
        }
    }
}