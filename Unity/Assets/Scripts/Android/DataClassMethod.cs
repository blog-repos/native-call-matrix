﻿using UnityEngine;

namespace Android
{
    public static class DataClassMethod
    {
        private class DataClass
        {
            public int dataInt;
            public bool dataBool;

#if UNITY_EDITOR || UNITY_ANDROID
            public AndroidJavaObject ToJavaObject()
            {
                var obj = new AndroidJavaObject("com.robinryf.nativecallmatrix.DataClass");
                obj.Set("dataInt", dataInt);
                obj.Set("dataBool", dataBool);
                return obj;
            }
#endif
            
        }
    
        public static void Call()
        {
#if UNITY_EDITOR || UNITY_ANDROID
            var data = new DataClass();
            data.dataInt = 10;
            data.dataBool = true;
        
            var nativeMethodsObj = new AndroidJavaObject("com.robinryf.nativecallmatrix.DataClassMethod");
            nativeMethodsObj.CallStatic("call", data.ToJavaObject());
#endif
        }
    }
}