﻿using UnityEngine;

namespace Android
{
    public static class IntMethod
    {
        public static void Call()
        {
#if UNITY_EDITOR || UNITY_ANDROID
            var nativeMethodsObj = new AndroidJavaObject("com.robinryf.nativecallmatrix.IntMethod");
            nativeMethodsObj.CallStatic("call", 42);
#endif
        }
    }
}